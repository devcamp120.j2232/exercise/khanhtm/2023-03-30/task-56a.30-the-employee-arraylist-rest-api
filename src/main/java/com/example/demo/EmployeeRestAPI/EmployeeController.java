package com.example.demo.EmployeeRestAPI;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
    public class EmployeeController {
        @GetMapping("/employees")
        public ArrayList<Employee> getEmployees() { 
            ArrayList<Employee> employees = new ArrayList<Employee>();

            Employee employees1 = new Employee(1 , "Trần","Khanh" , 40);
            Employee employees2 = new Employee(2 , "Nguyễn","Hoàng" , 60);
            Employee employees3 = new Employee(3 , "Nguyễn","Hoa" , 50);

            employees.add(employees1);
            employees.add(employees2);
            employees.add(employees3);

            return employees;
    }

}
